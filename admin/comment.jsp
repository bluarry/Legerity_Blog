<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/application.css">
    <script src="js/jquery-3.3.1.min.js" data-no-instant></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/editormd.min.css"/>
    <script src="js/editormd.min.js"></script>
    <script src="js/commentall.js"></script>
    <title>
        <s:text name="评论管理"/>
    </title>
</head>
<body>
<!--导航栏-->
<s:include value="header.jsp"/>
<!--导航栏结束 -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>评论管理</h1>
            </div>
            <ul class="nav nav-tabs">
                <li role="presentation" class="active"><a href="admin_comment.do">我的评论</a></li>
                <s:if test="#session.role==2 || #session.role==3">
                <li role="presentation"><a href="admin_commentreview.do">审核评论</a></li>
                </s:if>
            </ul>
        <s:if test="content.size()!=0">
            <table class="table">
                <thead>
                <tr>
                    <th>文章标题</th>
                    <th>评论内容</th>
                    <th>评论人</th>
                    <th>评论时间</th>
                    <th>评论人邮箱</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                
                <s:iterator value="content" id="item" status="art">
	                <tr id="item_<s:property value="#item.id" />">
	                    <th scope="row"><s:property value="#item.article.title" /></th>
	                    <td><s:property value="#item.comment" /></td>
	                    <td><s:property value="#item.user.userName" /></td>
	                    <td> <s:date name='#item.cmTime' format="yyyy-MM-dd HH:mm:ss"/></td>
	                    <td><s:property value="#item.user.email" /></td>
	                    <td>
	                    	<div class="btn-group">
	  							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	   											 操作 <span class="caret"></span>
	  							</button>
							  	<ul class="dropdown-menu">
							    	<li><a href='javascript:showmdfcommodal({"comid":<s:property value="id"/>});'>编辑</a></li>
                                    <li><a href='javascript:delcmbyid({"comid":<s:property value="#item.id" />});'>删除</a></li>
							  	</ul>
							</div>
	                    </td>
	                </tr>
                </s:iterator>
                </tbody>
            </table>
            
			  <nav class="pull-right">
	 				 <ul class="pagination">
	 				 
	 				 	<s:bean name="org.apache.struts2.util.Counter" id="counter">
	 				 	 <li>
	            			<a href="javascript:void(0);">
	           					<span >选择状态: </span>
		       						<select id="shenhe">
		       							<option <s:if test="reviewstatus==0">selected</s:if> value="0">未审核</option>
					       				<option <s:if test="reviewstatus==1">selected</s:if> value="1">审核通过</option>
										<option <s:if test="reviewstatus==2">selected</s:if> value="2">审核未通过</option>
					       			</select>
	       					</a>
	            		</li>
	 				 	
	 				 	
		 				 <li>
	            			<a href="javascript:void(0);">
	           					<span >每页显示条数: </span>
		       						<select id="fenye">
		       							<option <s:if test="pagesize==2">selected</s:if> value="2">2</option>
					       				<option <s:if test="pagesize==5">selected</s:if> value="5">5</option>
										<option <s:if test="pagesize==10">selected</s:if> value="10">10</option>
										<option <s:if test="pagesize==20">selected</s:if> value="20">20</option>
										<option <s:if test="pagesize==100">selected</s:if> value="100">100</option>
					       			</select>
	       					</a>
	            		</li>
	    				 <li <s:if test="currentpage==1">class="disabled"</s:if> >
                        	<a 	<s:if test="currentpage > 1"> href="admin_comment.do?currentpage=<s:text name='%{currentpage-1}'/>"</s:if>    
                        		aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    	</li>
	    				 <s:iterator var="counter" begin="1" end="allsize%pagesize==0?allsize/pagesize:allsize/pagesize+1" status="cc">
		    			  	<li <s:if test="#cc.count==currentpage">class="active"</s:if> >
                    			<a href="admin_comment.do?currentpage=<s:text name="#cc.count"/>" > <s:property value="top" /></a>
                    		</li>
	    				</s:iterator>
	    				  <li <s:if test="currentpage==(allsize%pagesize==0?allsize/pagesize:allsize/pagesize+1)">class="disabled"</s:if> >
                        <a <s:if test="currentpage!=(allsize%pagesize==0?allsize/pagesize:allsize/pagesize+1)"> href="admin_comment.do?currentpage=<s:text name="%{currentpage+1}"/>"</s:if> 
                        	aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
	    				</s:bean>
	  				</ul>
				</nav>
		</s:if>
		<s:else>
			 <div class="panel panel-default ">
               <div class="panel-heading">提示</div>
               <div class="panel-body">
               	<p>
               		<s:if test="reviewstatus==0"><s:text name="您还没有未审核评论"/></s:if>
               		<s:if test="reviewstatus==1"><s:text name="您还没有审核通过评论"/></s:if>
               		<s:if test="reviewstatus==2"><s:text name="您还没有审核未通过评论"/></s:if>
               	</p>
               </div>
          	</div>
			  <nav class="pull-right">
	 				 <ul class="pagination">
	 				 
	 				 	<s:bean name="org.apache.struts2.util.Counter" id="counter">
	 				 	 <li>
	            			<a href="javascript:void(0);">
	           					<span >选择状态: </span>
		       						<select id="shenhe">
		       							<option <s:if test="reviewstatus==0">selected</s:if> value="0">未审核</option>
					       				<option <s:if test="reviewstatus==1">selected</s:if> value="1">审核通过</option>
										<option <s:if test="reviewstatus==2">selected</s:if> value="2">审核未通过</option>
					       			</select>
	       					</a>
	            		</li>
	 				 	
	 				 	
		 				 <li>
	            			<a href="javascript:void(0);">
	           					<span >每页显示条数: </span>
		       						<select id="fenye">
		       							<option <s:if test="pagesize==2">selected</s:if> value="2">2</option>
					       				<option <s:if test="pagesize==5">selected</s:if> value="5">5</option>
										<option <s:if test="pagesize==10">selected</s:if> value="10">10</option>
										<option <s:if test="pagesize==20">selected</s:if> value="20">20</option>
										<option <s:if test="pagesize==100">selected</s:if> value="100">100</option>
					       			</select>
	       					</a>
	            		</li>
	    				 <li <s:if test="currentpage==1">class="disabled"</s:if> >
                        	<a 	<s:if test="currentpage > 1"> href="admin_userlist.do?currentpage=<s:text name="%{currentpage-1}"/>"</s:if>    
                        		aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    	</li>
	    				 <s:iterator var="counter" begin="1" end="allsize%pagesize==0?allsize/pagesize:allsize/pagesize+1" status="cc">
		    			  	<li <s:if test="#cc.count==currentpage">class="active"</s:if> >
                    			<a href="admin_userlist.do?currentpage=<s:text name="#cc.count"/>" > <s:property value="top" /></a>
                    		</li>
	    				</s:iterator>
	    				  <li <s:if test="currentpage==(allsize%pagesize==0?allsize/pagesize:allsize/pagesize+1)">class="disabled"</s:if> >
                        <a <s:if test="currentpage!=(allsize%pagesize==0?allsize/pagesize:allsize/pagesize+1)"> href="admin_userlist.do?currentpage=<s:text name="%{currentpage+1}"/>"</s:if> 
                        	aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
	    				</s:bean>
	  				</ul>
				</nav>
		</s:else>
        </div>
    </div>
</div>
<s:include value="models/modifycom.jsp" />


<script type="text/javascript">
	$("#backadmin_comment").addClass("active");
	$("#fenye").on("change",function(){
		var val=$(this).val();
		 window.location.href="admin_comment.do?currentpage=<s:text name='currentpage'/>&reviewstatus=<s:text name='reviewstatus'/>&pagesize="+val;
	});
	$("#shenhe").on("change",function(){
		var val=$(this).val();
		 window.location.href="admin_comment.do?currentpage=<s:text name='currentpage'/>&pagesize=<s:text name='pagesize'/>&reviewstatus="+val;
	});
	
</script>

</body>
</html>