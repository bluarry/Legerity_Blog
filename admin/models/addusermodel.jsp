<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<div class="modal fade" id="addUserModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">添加用户</h4>
            </div>
         <form id="myform" action="#" onsubmit="return check_form()">
            <div class="modal-body">
             
                    <div class="form-group">
                        <label for="uname">用户名</label>
                        <input type="text" id="uname" class="form-control" placeholder="用户名"/>
                    </div>
                    <div class="form-group">
                        <label for="pwd">密码</label>
                        <input type="text" id="pwd" class="form-control" placeholder="密码"/>
                    </div>
                    <div class="form-group">
                        <label for="pwd2">确认密码</label>
                        <input type="text" id="pwd2" class="form-control" placeholder="确认密码"/>
                    </div>
                    <div class="form-group">
                        <label for="email">邮箱</label>
                        <input type="email" id="email" class="form-control" placeholder="邮箱"/>
                    </div>
                    <div class="form-group">
                        <label for="ugroup">用户组</label>
                        <select id="ugroup" class="form-control">
                            <option value="1">普通用户</option>
                            <option value="2">管理员</option>
                        </select>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button id="ssubmit" type="submit" class="btn btn-primary">提交</button>
            </div>
       </form>
        </div>
    </div>
</div>

