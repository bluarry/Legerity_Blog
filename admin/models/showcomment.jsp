<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<div class="modal fade" id="showcomModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">查看评论</h4>
            </div>
            <div class="modal-body">
				<div class="form-group">
					  <label for="show_content">评论内容</label>
                      <textarea id="show_content" style=" resize: none;" readonly="readonly" rows="4" class="form-control"></textarea>
                </div>
                 <div><input id="show_hidenid" type="hidden" value=""></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

