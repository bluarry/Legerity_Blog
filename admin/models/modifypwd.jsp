<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<div class="modal fade" id="modifypwdModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">修改密码</h4>
            </div>
         <form id="modifypwdform" action="#" onsubmit="return check_mdpwd_form()">
            <div class="modal-body">
					<div class="form-group">
                        <label for="oldpwd">原始密码</label>
                        <input type="password" id="oldpwd" class="form-control" placeholder="密码"/>
                    </div>
                    <div class="form-group">
                        <label for="newpwd">密码</label>
                        <input type="password" id="newpwd" class="form-control" placeholder="密码"/>
                    </div>
                    <div class="form-group">
                        <label for="newpwd2">确认密码</label>
                        <input type="password" id="newpwd2" class="form-control" placeholder="确认密码"/>
                    </div>
                    <div><input id="hidenid" type="hidden" value=""></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button id="sssubmit" type="submit" class="btn btn-primary">提交</button>
            </div>
       </form>
        </div>
    </div>
</div>

