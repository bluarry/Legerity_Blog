<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<div class="modal fade" id="modifyartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">修改文章</h4>
            </div>
         <form id="modifyartform">
            <div class="modal-body">
				 
				 <div class="form-group">
                    <label for="mody_art_title">标题</label>
                    <input type="text" id="mody_art_title" name="title" class="form-control" placeholder="标题"/>
                </div>
                
                 <div class="editormd" id="mody_art_editor">
						<textarea class="editormd-markdown-textarea" name="test-editormd-markdown-doc"></textarea>
						<textarea class="editormd-html-textarea" name="text"></textarea>
                </div>
                <div><input id="art_hidenid" type="hidden" value=""></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button id="ssubmit" type="button" onclick="check_mdart_form()" class="btn btn-primary">提交</button>
            </div>
       </form>
        </div>
    </div>
</div>

