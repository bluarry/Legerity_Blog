<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/application.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/contentall.js"></script>
    <link rel="stylesheet" href="css/editormd.min.css"/>
    <script src="js/editormd.min.js"></script>
    
    <title>
        <s:text name="评论管理"/>
    </title>
</head>
<body>
<!--导航栏-->
<s:include value="header.jsp" />
<!--导航栏结束 -->
<div class="container">
    <div class="row">

        <div class="col-md-2">
            <div class="list-group">
                <a href="admin_content.do" class="list-group-item active">文章管理</a>
                <a href="admin_contentpost.do" class="list-group-item">添加文章</a>
            </div>
        </div>

        <div class="col-md-10">
            <div class="page-header">
                <h1>文章管理</h1>
            </div>

            <ul class="nav nav-tabs">
                <li role="presentation" class="active"><a href="#">文章列表</a></li>
            </ul>
		<s:if test="content.size()!=0">
            <table class="table">
                <thead>
                <tr>
                    <th>文章标题</th>
                    <th>作者</th>
                    <th>发布时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <s:iterator value="content" id="item" status="art">
	                <tr id="item_<s:property value="#item.id"/>">
	                    <th scope="row"><s:property value="title" /></th>
	                    <td><s:property value="user.getUserName()" /></td>
	                    <td>   <s:date name='#item.publishtime' format="yyyy-MM-dd HH:mm:ss"/> </td>
	                    <td>
	                    	<div class="btn-group">
	  							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	   											 操作 <span class="caret"></span>
	  							</button>
								  <ul class="dropdown-menu">
								    	
								    	<li><a href='javascript:showmdfartmodal({"artid":<s:property value="id"/>});'>编辑</a></li>
								    	
	                                   	<li><a href='javascript:art_del({"artid":<s:property value="id"/>});'>删除</a></li>
								  
								  </ul>
							</div>
	                    </td>
	                    
	                </tr>
				</s:iterator>
                </tbody>
            </table>
            
            <!-- 分页 -->
            <nav class="pull-right ">
                <ul class="pagination">
                <s:bean name="org.apache.struts2.util.Counter" id="counter">
					
                	<li>
            			<a href="javascript:void(0);">
           					<span >每页显示条数: </span>
	       						<select>
	       							<option <s:if test="pagesize==2">selected</s:if> value="2">2</option>
				       				<option <s:if test="pagesize==5">selected</s:if> value="5">5</option>
									<option <s:if test="pagesize==10">selected</s:if> value="10">10</option>
									<option <s:if test="pagesize==20">selected</s:if> value="20">20</option>
									<option <s:if test="pagesize==100">selected</s:if> value="100">100</option>
				       			</select>
       					</a>
            		</li>
            		<!-- begin -->
            	
            		
                    <li <s:if test="currentpage==1">class="disabled"</s:if> >
                        <a 	<s:if test="currentpage > 1"> href="admin_content.do?currentpage=<s:text name="%{currentpage-1}"/>"</s:if>    
                        		aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                     
                    <s:iterator var="counter" begin="1" end="allsize%pagesize==0?allsize/pagesize:allsize/pagesize+1" status="cc">
                    
                    <li <s:if test="#cc.count==currentpage">class="active"</s:if> >
                    	<a href="admin_content.do?currentpage=<s:text name="#cc.count"/>" > <s:property value="top" /></a>
                    </li>
                     </s:iterator>
                    <li <s:if test="currentpage==(allsize%pagesize==0?allsize/pagesize:allsize/pagesize+1)">class="disabled"</s:if>
                    >
                        <a <s:if test="currentpage!=(allsize%pagesize==0?allsize/pagesize:allsize/pagesize+1)"> href="admin_content.do?currentpage=<s:text name="%{currentpage+1}"/>"</s:if> 
                        	aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    
                  
				</s:bean>
                <!-- end -->
                </ul>
            </nav>
            </s:if>
            <s:else>
            	<div class="panel panel-default">
	                <div class="panel-heading">提示</div>
	                <div class="panel-body">
	                    <p><s:text name="文章列表为空"/></p>
	                </div>
            	</div>
            </s:else>
        </div>
    </div>
</div>
<s:include value="models/modifyart.jsp"/>


<script type="text/javascript">
	$("#backadmin_article").addClass("active");
	$("select").on("change",function(){
		var val=$(this).val();
		 window.location.href="admin_content.do?currentpage=<s:text name='currentpage'/>&pagesize="+val;
	});
	
	
</script>
</body>
</html>