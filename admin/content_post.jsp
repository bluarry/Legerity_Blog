<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
 	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/application.css">
    <link href="css/bootstrapValidator.css" rel="stylesheet">
    
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/editormd.min.css"/>
    <script src="js/editormd.min.js"></script>
    <script src="js/bootstrapValidator.min.js"></script>
    <title>
        <s:text name="文章管理"/>
    </title>
</head>
<body>
<!--导航栏-->
<s:include value="header.jsp" />
<!--导航栏结束 -->
<div class="container">
    <div class="row">

        <div class="col-md-2">
            <div class="list-group">
                <a href="admin_content.do" class="list-group-item">文章管理</a>
                <a href="admin_contentpost.do" class="list-group-item active">添加内容</a>
            </div>
        </div>

        <div class="col-md-10">
            <div class="page-header">
                <h1>文章管理</h1>
            </div>

            <ul class="nav nav-tabs">
                <li role="presentation" class="active"><a href="#">文章列表</a></li>
            </ul>

            <form id="articleform">
                <div class="form-group">
                    <label for="title">标题</label>
                    <input type="text" id="title" name="title" class="form-control" placeholder="标题"/>
                </div>
                <div class="form-group" id="editor">
                    <label for="content">正文</label>
                    <textarea  id="content" name="content"  style="display:none;" class="form-control" cols="10" placeholder="请输入正文"></textarea>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox"/> 置顶
                    </label>
                    <button type="button" id="publish" class="btn btn-default pull-right">发布文章</button>
                </div>
            </form>
        </div>
    </div>
</div>
	
<script type="text/javascript">
	//导航栏设置active
	$("#backadmin_article").addClass("active");
    //    调用编辑器
    var testEditor;
    $(function() {
        testEditor = editormd("editor", {
            height  : 640,
            syncScrolling : "single",
            path    : "editormd/lib/",
        });
    });
    /*Ajax异步传输文章*/
    $(document).ready(function(){
    	 $("#publish").click(function () {
    		 if($("#title").val()=="" || $("#content").val()==""){
    			 alert("标题或内容不能为空");
    			 return ;
    		 }
    		  $.ajax({
    			  type:"post",
                  cache:false,
                  url:"SaveArticle.do",
                  dataType:"json",
                  data:$('#articleform').serialize(),
                  async:true,
                  error:function(request) {
                	  alert("出错");
                  },
                  success:function(data) {
                      if(data['result']){
                    	  alert("发表文章成功了");
                    	  $("#title").val("");
                    	  testEditor.setValue("");
                      }else{
                    	  alert("出错咯");
                      }
                  }
    			})
    		 
    	 });
    });
    
 </script>
</body>
</html>