<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/application.css">
    <script src="js/jquery-3.3.1.min.js" ></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/modifypass.js"></script>
    
    <title>
        <s:text name="后台"/>
    </title>
</head>
<body>
	<!-- start 导航栏 -->
	<s:include value="header.jsp"/>
	<!-- end 导航栏 -->
	
    <div class="container">
        <!--这里写统计信息,和当前热门 -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">博客统计数据</div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>统计项目</th>
                                    <th>昨日</th>
                                    <th>今日</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">访客数目</th>
                                    <td>10</td>
                                    <td>0</td>
                                </tr>
                                <tr>
                                    <th scope="row">文章数目</th>
                                    <td>10</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">博客热门文章</div>
                    <ul class="list-group">
                        <li class="list-group-item"><a href="#"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;文章一 <small class="pull-right">2018/5/16</small></a></li>
                        <li class="list-group-item"><a href="#"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;文章一<small class="pull-right">2018/5/16</small></a></li>
                        <li class="list-group-item"><a href="#"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;文章一<small class="pull-right">2018/5/16</small></a></li>
                        <li class="list-group-item"><a href="#"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;文章一<small class="pull-right">2018/5/16</small></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    
<!-- 后面有一个留言板，需要的话再说-->
<script type="text/javascript">
	$("#backadmin_index").addClass("active");
</script>
</body>
</html>