<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>

 <script src="js/modifypass.js"></script>
   <link href="css/myheader.css" rel="stylesheet">
<!-- start 头图片 -->
<header class="main-header iheaderstyle">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>博客系统</h1>
			</div>
		</div>
	</div>
</header>
<!-- end 头图片 -->
  <!--导航栏-->
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">后台管理</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin_index.do">后台管理</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                	<li><a href="index.do" ><span class="glyphicon glyphicon-screenshot"></span>&nbsp;&nbsp;前台首页</a></li>
                	<s:if test="#session.role==1">
                	<li id="backadmin_index"><a href="admin_index.do"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;后台首页</a></li>
                	<li id="backadmin_comment"><a href="admin_comment.do"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;评论管理</a></li>
                	</s:if>
                    <s:elseif test="#session.role==2">
                    <li id="backadmin_index"><a href="admin_index.do"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;后台首页</a></li>
                   	<li id="backadmin_comment"><a href="admin_comment.do"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;评论管理</a></li>
                    <li  id="backadmin_user"><a href="admin_userlist.do"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;用户管理</a></li>
                    </s:elseif>
                    <s:elseif test="#session.role==3">
                     <li id="backadmin_index"><a href="admin_index.do"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;后台首页</a></li>
                    <li id="backadmin_article"><a href="admin_content.do"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;文章管理</a></li>
                    <li id="backadmin_comment"><a href="admin_comment.do"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;评论管理</a></li>
                    <li  id="backadmin_user"><a href="admin_userlist.do"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;用户管理</a></li>
                    </s:elseif>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <s:text name="%{#session.uname}" />
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                        	<%-- 
                            <li><a href="#"><span class="glyphicon glyphicon-flag"></span>&nbsp;&nbsp;个人信息</a></li>
                        	--%>
                            <li><a href='javascript:showmdfpwdmodal({"userid":<s:property value="#session.uid"/>});'><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;密码修改</a></li>
                        </ul>
                    </li>
                   
                    <li><a href="logout.do?baction=logout"><span class="glyphicon glyphicon-off"></span>&nbsp;&nbsp;退出</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!--导航栏结束 -->
     <s:include value="models/modifypwd.jsp" />