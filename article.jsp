<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/screen.css" rel="stylesheet">
    <link href="css/application.css" rel="stylesheet">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="editormd/lib/marked.min.js"></script>
    <script src="editormd/lib/prettify.min.js"></script>
    <script src="editormd/lib/raphael.min.js"></script>
    <script src="editormd/lib/underscore.min.js"></script>
    <script src="editormd/lib/sequence-diagram.min.js"></script>
    <script src="editormd/lib/flowchart.min.js"></script>
    <script src="js/editormd.min.js"></script>


    <script src="js/jquery.comment.js"></script>

    <title>
        <s:text name="文章"/>
    </title>
</head>
<body>
<s:include value="index_header.jsp"/>

<!-- start 主界面布局 -->
<section class="content-wrap">
    <div class="container">
        <div class="row">
            <!--文章列表-->
            <main class="col-md-9">
                <article id="1" class="post">
                    <div class="post-head">
                        <h1 class="post-title">
                            <s:property value="art.title"/>
                        </h1>
                        <div class="post-meta">
                            <span class="author">作者：<a href="#"> <s:property value="art.user.userName"/> </a></span>
                            &bull;
                            <time class="post-date">
                                <s:date name='art.publishtime' format="yyyy-MM-dd HH:mm:ss"/>
                            </time>
                        </div>
                    </div>
                    <div id="contentman" class="post-content">
                        <textarea style="display:none;">${art.content}</textarea>
                    </div>
                </article>
               
                
                <article>
                 <s:if test="#session.role!=null && #session.uname!=null && #session.banstatus==0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                <textarea id="content" style=" resize: none;" rows="3" class="form-control"
                                          placeholder="来说几句吧......"></textarea>
                                <span class="input-group-btn">
                                <button id="comment" class="btn btn-primary" type="button">评论</button>
                              </span>
                            </div>
                        </div>
                    </div>
                   </s:if>
                   <s:elseif test="#session.role!=null && #session.uname!=null &&#session.banstatus==1">
                   	 <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                               
	                                <div class="panel-body">
	                                    <h3><s:text name="对不起，您已被禁言，无法发表评论，请联系管理员"/></h3>
	                                </div>
                               </div>
                          	</div>
                       </div>
                   </s:elseif>
                   <s:else>
                       <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                               
	                                <div class="panel-body">
	                                    <h3><s:text name="请登录后发表评论"/></h3>
	                                </div>
                               </div>
                          	</div>
                       </div>
                   </s:else>
                    <div class="comment-list"></div>
                </article>
            </main>
            <!--侧边栏-->
            <aside class="col-md-3 sidebar">
                <div class="widget">
                    <h4 class="title">热门文章</h4>
                    <div class="content community">
                        <p><a href="#" title="文章标题" target="_blank">第一篇文章</a></p>
                        <p><a href="#" title="文章标题" target="_blank">第一篇文章</a></p>
                    </div>
                </div>
               
            </aside>
        </div>
    </div>
</section>

<script type="text/javascript">
    var testEditor;
    $(function () {
        testEditor = editormd.markdownToHTML("contentman", {//注意：这里是上面DIV的id
            htmlDecode: "style,script,iframe",
            emoji: true,
            taskList: true,
            tex: true, // 默认不解析
            flowChart: true, // 默认不解析
            sequenceDiagram: true, // 默认不解析
            codeFold: true,
        });
    });


    /*这里是评论*/
    var arr = [];

    $(function () {

        /*通过ajax获取后台的评论信息*/
        $.ajax({
            type: "post",
            cache: false,
            url: "getcmt.do",
            dataType: "json",
            data: "artid=<s:property value='art.id' />",
            async:
                true,
            error:function (request) {
                  
                },
            success: function (data) {
                arr = [];
               
                /*
                 {
                       id:3,
                       img:"images/logo.png",
                       replyName:"帅大叔",
                       beReplyName:"匿名",
                       content:"同学聚会，看到当年追我的屌丝开着宝马车带着他老婆来了，他老婆是我隔壁宿舍的同班同学，心里后悔极了。",
                       time:"2017-10-17 11:42:53",
                       address:"深圳",
                       osname:"win10",
                       browse:"谷歌",
                       replyBody:[]
                   }	 
                */
                for (var x in data) {
                	var oobj = {};
                    oobj.id = data[x].id;
                    oobj.img = "images/logo.png";
                    oobj.replyName = data[x].user.userName;
                    oobj.content = data[x].comment;
                    oobj.time = data[x].cmTime;
                    oobj.address = data[x].address;
                    oobj.osname = data[x].osname;
                    oobj.browse = data[x].browse;
                    arr.push(oobj);
                }
              
                $(".comment-list").addCommentList({data: arr, add: ""});
            }
        })

        $("#comment").click(function () {
            var obj = new Object();
            obj.img = "images/logo.png";
            obj.replyName = "<s:property value='#session.uname' />";
            obj.content = $("#content").val();
            obj.articleid = "<s:property value='art.id' />";

            $(".comment-list").addCommentList({data: [], add: obj});
            $("#content").val("");

        });

    })

</script>
</body>
</html>