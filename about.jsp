<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/screen.css" rel="stylesheet">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
	<script src="editormd/lib/marked.min.js"></script>
	<script src="editormd/lib/prettify.min.js"></script>
	<script src="editormd/lib/raphael.min.js"></script>
	<script src="editormd/lib/underscore.min.js"></script>
	<script src="editormd/lib/sequence-diagram.min.js"></script>
	<script src="editormd/lib/flowchart.min.js"></script>
	<script src="js/editormd.min.js"></script>
	
    <title>
        <s:text name="关于"/>
    </title>
</head>
<body>
<s:include value="index_header.jsp"/>

<!-- start 主界面布局 -->
<section class="content-wrap">
    <div class="container">
        <div class="row">
            <!--文章列表-->
            <main class="col-md-9">
                <article id="1" class="post">
                    <div class="post-head">
                        <h1 class="post-title">
                           		<s:property value="art.title" />
                        </h1>
                        <div class="post-meta">
                            <span class="author">作者：<a href="#"> <s:property value="art.user.userName" /> </a></span> &bull;
                            <time class="post-date">
                               <s:date name='art.publishtime' format="yyyy-MM-dd HH:mm:ss"/>
                            </time>
                        </div>
                    </div>
                    <div id="contentman" class="post-content">
                    	   <textarea style="display:none;">${art.content}</textarea>
                    </div>
                </article>

            </main>
            <!--侧边栏-->
            <aside class="col-md-3 sidebar">
                <div class="widget">
                    <h4 class="title">热门文章</h4>
                    <div class="content community">
                        <p><a href="#" title="文章标题" target="_blank">第一篇文章</a></p>
                        <p><a href="#" title="文章标题" target="_blank" >第一篇文章</a></p>
                    </div>
                </div>
                <div class="widget">
                    <h4 class="title">标签</h4>
                    <div class="content tag-cloud">
                        <a href="#">客户端</a>
                        <a href="#">Android</a>
                        <a href="#">学习</a>
                        <a href="#">算法</a>
                    </div>
            </aside>
        </div>
    </div>
</section>

<script type="text/javascript">
	var testEditor;
	$(function () {
	    testEditor = editormd.markdownToHTML("contentman", {//注意：这里是上面DIV的id
	        htmlDecode: "style,script,iframe",
	        emoji: true,
	        taskList: true,
	        tex: true, // 默认不解析
	        flowChart: true, // 默认不解析
	        sequenceDiagram: true, // 默认不解析
	        codeFold: true,
	});});
	
	 $(document).ready(function() {
		 $("#Menu_index").removeClass("active");
		 $("#Menu_about").addClass("active");
	 })
	
</script>
</body>
</html>