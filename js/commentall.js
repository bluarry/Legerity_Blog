 /*字符串格式化*/
String.prototype.format = function(args) {
	var result = this;
	if (arguments.length < 1) {
		return result;
	}

	var data = arguments; // 如果模板参数是数组
	if (arguments.length == 1 && typeof (args) == "object") {
		// 如果模板参数是对象
		data = args;
	}
	for ( var key in data) {
		var value = data[key];
		if (undefined != value) {
			result = result.replace("{" + key + "}", value);
		}
	}
	return result;
}

function showmdfcommodal(obj){
	var comid=obj.comid;
	$("#com_hidenid").val(comid);

	$.ajax({
		 type:"post",
        cache:false,
        url:"Getcommentbyid.do",
        dataType:"json",
        data:{"id":comid},
        async:true,
        error:function(request) {
        	alert("服务器内部错误，请稍后重试 ");
        },
        success:function(data){
        	$("#com_content").val(data[1].comment);
        	$("#modifycomModal").modal();
        }
	});		
}

function check_com_form(){
	var comid=$("#com_hidenid").val();
	var comment=$("#com_content").val();
	if(comment.length < 4){
		alert("评论过短");
		return ;
	}
	$.ajax({
		type:"post",
		cache:false,
		url:"Updatecomment.do",
		dataType:"json",
		data:{"id":comid,"comment":comment},
		async:true,
		error:function(request) {
			alert("服务器内部错误，请稍后重试 ");
		},
		success:function(data){
    	   if(data[1]){
    		   alert("修改成功");
    		   location.reload();
    	   }else{
    		   alert("修改失败");
    		   location.reload();
    	   }
		}
	});		
}

function delcmbyid(obj){
	var cmid=obj.comid;
	$.ajax({
		type:"post",
		cache:false,
		url:"Delcombyid.do",
		dataType:"json",
		data:{"id":cmid},
		async:true,
		error:function(request) {
			alert("服务器内部错误，请稍后重试 ");
		},
		success:function(data){
    	   if(data[1]){
    			var a="item_{0}".format(cmid);
        		$('#{0}'.format(a)).remove();
    	   }else{
    		   alert("修改失败");
    	   }
		}
	});		
}
