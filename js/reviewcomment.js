 /*字符串格式化*/
String.prototype.format = function(args) {
	var result = this;
	if (arguments.length < 1) {
		return result;
	}

	var data = arguments; // 如果模板参数是数组
	if (arguments.length == 1 && typeof (args) == "object") {
		// 如果模板参数是对象
		data = args;
	}
	for ( var key in data) {
		var value = data[key];
		if (undefined != value) {
			result = result.replace("{" + key + "}", value);
		}
	}
	return result;
}

function showmdfcommodal(obj){
	var comid=obj.comid;
	$.ajax({
		 type:"post",
        cache:false,
        url:"Getcommentbyid.do",
        dataType:"json",
        data:{"id":comid},
        async:true,
        error:function(request) {
        	alert("服务器内部错误，请稍后重试 ");
        },
        success:function(data){
        	$("#show_content").val(data[1].comment);
        	$("#showcomModal").modal();
        }
	});		
}
function reviewcomment(obj){
	var comid=obj.comid;
	var acstr=obj.acstr;
	
	$.ajax({
		type:"post",
		cache:false,
		url:"Reviewcom.do",
		dataType:"json",
		data:{"id":comid,"acstr":acstr},
		async:true,
		error:function(request) {
			alert("服务器内部错误，请稍后重试 ");
		},
		success:function(data){
    	   if(data[1]){
    			var a="item_{0}".format(comid);
        		$('#{0}'.format(a)).remove();
    	   }else{
    		   alert("审核失败");
    	   }
		}
	});
	
	
	
}
