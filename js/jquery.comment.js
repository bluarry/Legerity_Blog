(function($) {
	function crateCommentInfo(obj) {

		if (typeof (obj.time) == "undefined" || obj.time == "") {
			obj.time = getNowDateFormat();
		}

		var el = "<div class='comment-info'><header><img src='"
				+ obj.img
				+ "'></header><div class='comment-right'><h3>"
				+ obj.replyName
				+ "</h3>"
				+ "<div class='comment-content-header'><span><i class='glyphicon glyphicon-time'></i>"
				+ obj.time + "</span>";

		if (typeof (obj.address) != "undefined" && obj.browse != "") {
			el = el + "<span><i class='glyphicon glyphicon-map-marker'></i>"
					+ obj.address + "</span>";
		}
		el = el
				+ "</div><p class='content'>"
				+ obj.content
				+ "</p><div class='comment-content-footer'><div class='row'><div class='col-md-10'>";

		if (typeof (obj.osname) != "undefined" && obj.osname != "") {
			el = el + "<span><i class='glyphicon glyphicon-pushpin'></i> 来自:"
					+ obj.osname + "</span>";
		}

		if (typeof (obj.browse) != "undefined" && obj.browse != "") {
			el = el + "<span><i class='glyphicon glyphicon-globe'></i> "
					+ obj.browse + "</span>";
		}

		el = el + "</div></div></div><div class='reply-list'>";
		el = el + "</div></div></div>";

		return el;
	}

	function getNowDateFormat() {
		var nowDate = new Date();
		var year = nowDate.getFullYear();
		var month = filterNum(nowDate.getMonth() + 1);
		var day = filterNum(nowDate.getDate());
		var hours = filterNum(nowDate.getHours());
		var min = filterNum(nowDate.getMinutes());
		var seconds = filterNum(nowDate.getSeconds());
		return year + "-" + month + "-" + day + " " + hours + ":" + min + ":"
				+ seconds;
	}
	function filterNum(num) {
		if (num < 10) {
			return "0" + num;
		} else {
			return num;
		}
	}
	var _address = "";
	$.getScript('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js', function() {
				_address = remote_ip_info.city;
			});

	function getbrowser() {
		var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  
		var isOpera = userAgent.indexOf("Opera") > -1;
		//判断是否Opera浏览器  
		if (isOpera) {
			return "Opera"
		}
		;
		//判断是否Firefox浏览器  
		if (userAgent.indexOf("Firefox") > -1) {
			return "Firefox";
		}
		//判断是否chorme浏览器  
		if (userAgent.indexOf("Chrome") > -1) {
			return "Chrome";
		}
		//判断是否Safari浏览器  
		if (userAgent.indexOf("Safari") > -1) {
			return "Safari";
		}
		//判断是否IE浏览器  
		if (userAgent.indexOf("compatible") > -1
				&& userAgent.indexOf("MSIE") > -1 && !isOpera) {
			return "IE";
		}
		//判断是否Edge浏览器  
		if (userAgent.indexOf("Trident") > -1) {
			return "Edge";
		}
		;
	}
	function check_os() {
		windows = (navigator.userAgent.indexOf("Windows", 0) != -1) ? 1 : 0;
		mac = (navigator.userAgent.indexOf("mac", 0) != -1) ? 1 : 0;
		linux = (navigator.userAgent.indexOf("Linux", 0) != -1) ? 1 : 0;
		unix = (navigator.userAgent.indexOf("X11", 0) != -1) ? 1 : 0;
		if (windows)
			os_type = "Windows";
		else if (mac)
			os_type = "Apple Mac";
		else if (linux)
			os_type = "Lunux";
		else if (unix)
			os_type = "Unix";
		return os_type;
	}
	
	function sleep(n) { //n表示的毫秒数
         var start = new Date().getTime();
         while (true) if (new Date().getTime() - start > n) break;
     }
	var cc;
	$.fn.addCommentList = function(options) {
		var defaults = {
			data : [],
			add : ""
		}
		var option = $.extend(defaults, options);
		// 加载数据
		if (option.data.length > 0) {
			var dataList = option.data;
			var totalString = "";
			for (var i = 0; i < dataList.length; i++) {
				var obj = dataList[i];
				var objString = crateCommentInfo(obj);
				totalString = totalString + objString;
			}
			$(this).append(totalString);
		}
		cc=this;
		// 添加新数据
		if (option.add != "") {
			obj = option.add;
			obj.address = _address;
			obj.browse = getbrowser();
			obj.osname=check_os();
			var str = crateCommentInfo(obj);
			
			
			//console.log(obj);
			  $.ajax({
				  type:"post",
		             cache:false,
		             url:"savecmt.do",
		             dataType:"json",
		             data:obj,
		             async:true,
		             error:function(request) {
		           	  alert("服务端出错，保存评论错误");
		             },
		             success:function(data){
		            	 if(data['result'])
		            	 {
		            		 alert("评论成功");
		            		 $(cc).prepend(str);
		            	 }else{
		            		 alert("评论失败,未知错误");
		            	 }
		             }
				})
		}
	}

})(jQuery);