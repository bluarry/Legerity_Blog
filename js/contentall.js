 /*字符串格式化*/
String.prototype.format = function(args) {
	var result = this;
	if (arguments.length < 1) {
		return result;
	}

	var data = arguments; // 如果模板参数是数组
	if (arguments.length == 1 && typeof (args) == "object") {
		// 如果模板参数是对象
		data = args;
	}
	for ( var key in data) {
		var value = data[key];
		if (undefined != value) {
			result = result.replace("{" + key + "}", value);
		}
	}
	return result;
}

/*删除文章*/
function art_del(obj){
	var artid=obj.artid;
	 
	$.ajax({
		 type:"post",
         cache:false,
         url:"Delartbyid.do",
         dataType:"json",
         data:{"id":artid},
         async:true,
         error:function(request) {},
         success:function(data){
        	 console.log(data);
        	if(data[1]){
        		var a="item_{0}".format(artid);
        		$('#{0}'.format(a)).remove();
        	}else{
        		alert("删除失败");
        	}
         }
		});		
}

/*编辑文章*/
function showmdfartmodal(obj){
	var artid=obj.artid;
	$("#art_hidenid").val(obj.artid);
	$("#modifyartModal").modal();
	 var mmEditor;
	 $(function() {
	        mmEditor = editormd("mody_art_editor", {
	            height  : 640,
	            syncScrolling : "single",
	            path    : "editormd/lib/",
	            watch : false, 
	        });
	  });

	 $.ajax({
		 type:"post",
         cache:false,
         url:"Getartbyid.do",
         dataType:"json",
         data:{"id":artid},
         async:true,
         error:function(request) {},
         success:function(data){
        	if(data==null){
        		alert("出错");
        		location.reload();
        	}
        	$('#mody_art_title').val(data[1].title);
       	 	$('.editormd-markdown-textarea').val(data[1].content);
         }
		});		

}

function check_mdart_form(){
	var artid=$("#art_hidenid").val();
	var title=$('#mody_art_title').val();
	var content=$('.editormd-markdown-textarea').val();
	
	 $.ajax({
		 type:"post",
         cache:false,
         url:"Updateartbyid.do",
         dataType:"json",
         data:{"id":artid,"title":title,"content":content},
         async:true,
         error:function(request) {
        	 alert("服务器内部出错，请稍后重试");
         },
         success:function(data){
        	/*console.log(data);*/
        	if(data[1]){
        		alert("修改成功");
        		location.reload();
        	}else{
        		alert("修改失败,请重试");
        	}
         }
		});		
	
	return false;
}