/*修改密码模态对话框*/
function showmdfpwdmodal(obj){
	$("#hidenid").val(obj.userid);
	$("#modifypwdModal").modal();
}
function check_mdpwd_form(){
	
	var oldpwd= $.trim($('#oldpwd').val());
	var newpwd= $.trim($('#newpwd').val());
    var newpwd2= $.trim($('#newpwd2').val());
	
    if(oldpwd.length==0){
    	alert("原始密码不能为空");
    	return false;
    }
    if(newpwd.length<4){
    	alert("密码太短");
    	return false;
    }
    if(newpwd=="" || newpwd2==""){
		alert("密码不能为空");
		return false;
	}
    
	if(newpwd!=newpwd2){
    	alert("新密码不匹配");
    	return false;
    }
    
    $.ajax({
    	type:"post",
    	cache:false,
    	url:"Modifypwd.do",
    	dataType:"json",
    	data:{"id":$("#hidenid").val(),"oldpwd":oldpwd,"newpwd":newpwd},
    	async:true,
    	error:function(request) {},
    	success:function(data){
    		if(data['oldpass']==false){
    			alert("用户密码错误");
    			location.reload();
    			return ;
    		}
    		
    		if(data['modify']){
    			alert("密码修改成功");
    			location.reload();
    		}else{
    			alert("密码修改失败");
    			location.reload();
    		}
    		return ;
    	}
    });		
    
    return false;
}
