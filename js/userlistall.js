 /*字符串格式化*/
String.prototype.format = function(args) {
	var result = this;
	if (arguments.length < 1) {
		return result;
	}

	var data = arguments; // 如果模板参数是数组
	if (arguments.length == 1 && typeof (args) == "object") {
		// 如果模板参数是对象
		data = args;
	}
	for ( var key in data) {
		var value = data[key];
		if (undefined != value) {
			result = result.replace("{" + key + "}", value);
		}
	}
	return result;
}
/*添加用户*/
function check_form(){
		var pwd= $.trim($('#pwd').val());
	    var pwd2= $.trim($('#pwd2').val());
	    var email= $.trim($('#email').val());
	    var ugroup = $('#ugroup').val();
	    
	    if($('#uname').val().length<4){
	    	alert("用户名太短");
	    	return false;
	    }
	    if(pwd.length==0|| pwd2.length==0){
	    	alert("密码不能为空");
	    	return false;
	    }
	    if(pwd!=pwd2){
	    	alert("密码不匹配");
	    	return false;
	    }
	    /*验证用户名*/
  		$.ajax({
 			  type:"post",
               cache:false,
               url:"UCAction.do",
               dataType:"json",
               data:{uname:$('#uname').val()},
               async:true,
               error:function(request) {},
               success:function(data){
              	 if(data["valid"]==false){
              		 alert("用户名已被注册");
              		  location.reload();
              	 	}
               }
 		})	
 		var arr={};
	    arr.uname=$.trim($('#uname').val());
	    arr.pwd=pwd;
	    arr.pwd2=pwd2;
	    arr.email=email;
	    arr.ugroup=ugroup;
  		
	    $.ajax({
			 type:"post",
             cache:false,
             url:"adduser.do",
             dataType:"json",
             data:arr,
             async:true,
             error:function(request) {},
             success:function(data){
            	if(data["result"])
            	{
            		alert("添加成功");
                	location.reload();
            	}else{
            		alert("添加失败");
                	location.reload();
            	}
            	
             }
		})	
	    
	    return false;
	}

function user_del(obj){
	var userid=obj.userid;
	 $.ajax({
		  type:"post",
         cache:false,
         url:"Deluserbyid.do",
         dataType:"json",
         data:{"id":userid},
         async:true,
         error:function(request) {},
         success:function(data){
        	if(data[1]){
        		var a="item_{0}".format(userid);
        		$('#{0}'.format(a)).remove();
        	}else{
        		alert("删除失败");
        	}
         }
		});		
	
}
function user_ban(obj){
	var userid=obj.userid;
	 $.ajax({
		  type:"post",
         cache:false,
         url:"Banuserbyid.do",
         dataType:"json",
         data:{"id":userid,"acstr":"ban"},
         async:true,
         error:function(request) {},
         success:function(data){
        	if(data[1]){
        		alert("禁言成功");
        		var id="banst{0}".format(userid);
        		
        		var rht="<li id={0}>".format(id)+
        			  "<a href=" +
        			  "javascript:user_freeban({'userid':{0}});".format(userid)+
        			  ">解除禁言</a></li>"
        		$("#"+id).replaceWith(rht);
        	}else{
        		alert("禁言失败");
        	}
         }
		});		
	
}
function user_freeban(obj){
	var userid=obj.userid;
	 $.ajax({
		  type:"post",
         cache:false,
         url:"Banuserbyid.do",
         dataType:"json",
         data:{"id":userid,"acstr":"freeban"},
         async:true,
         error:function(request) {},
         success:function(data){
        	if(data[1]){
        		alert("解禁成功");
        		var id="banst{0}".format(userid);
        		
        		var rht="<li id={0}>".format(id)+
        			  "<a href=" +
        			  "javascript:user_ban({'userid':{0}});".format(userid)+
        			  ">禁言</a></li>"
        	    $("#"+id).replaceWith(rht);
        	}else{
        		alert("解禁失败");
        	}
         }
		});		
	
}


function user_setadmin(obj){
	var userid=obj.userid;

	 $.ajax({
		  type:"post",
         cache:false,
         url:"Setadminbyid.do",
         dataType:"json",
         data:{"id":userid,"acstr":obj.acstr},
         async:true,
         error:function(request) {},
         success:function(data){
        	if(data[1]){
        		alert("设置成功");
        		$("#item_{0}".format(userid)).remove();
        	}else{
        		alert("设置失败");
        	}
         }
		});		
}

