
var articlecount=0;
/*字符串格式化*/
String.prototype.format = function(args) {
	var result = this;
	if (arguments.length < 1) {
		return result;
	}

	var data = arguments; // 如果模板参数是数组
	if (arguments.length == 1 && typeof (args) == "object") {
		// 如果模板参数是对象
		data = args;
	}
	for ( var key in data) {
		var value = data[key];
		if (undefined != value) {
			result = result.replace("{" + key + "}", value);
		}
	}
	return result;
}
/*格式化日期*/
function formatDate(date, format) {
	
	if (!date)
		return;
	if (!format)
		format = "yyyy-MM-dd";
	switch (typeof date) {
	case "string":
		date = new Date(date.replace(/-/, "/"));
		break;
	case "number":
		date = new Date(date);
		break;
	}
	if (!date instanceof Date)
		return;
	//console.log(date);
	var dict = {
		"yyyy" : date.getFullYear(),
		"M" : date.getMonth() + 1,
		"d" : date.getDate(),
		"H" : date.getHours(),
		"m" : date.getMinutes(),
		"s" : date.getSeconds(),
		"MM" : ("" + (date.getMonth() + 101)).substr(1),
		"dd" : ("" + (date.getDate() + 100)).substr(1),
		"HH" : ("" + (date.getHours() + 100)).substr(1),
		"mm" : ("" + (date.getMinutes() + 100)).substr(1),
		"ss" : ("" + (date.getSeconds() + 100)).substr(1)
	};
	return format.replace(/(yyyy|MM?|dd?|HH?|ss?|mm?)/g, function() {
		return dict[arguments[0]];
	});
}
/*
 * 生成简要介绍
 * */
function brief(content){
	content=content.substring(0,30);
	content=content.replace(/#/g," ");
	content=content.replace(/>/g," ");
	content=content.replace(/\*/g," ");	
	return content;
}

/*
 * 这里加载后台以json返回的文章
 * */
function dealjsonArticle(data) {
	 
	//console.log(data);
	
	for(x in data){
		//console.log(data[x].publishtime);
		
		var xx=
			'<article id="{0}" class="post">'.format(1)+
				'<div class="post-head">'	+
					'<h1 class="post-title">'+
						'<a href="Showarticle.do?id={0}">{1} </a>'.format(data[x].id,data[x].title)+
					'</h1>'+
					'<div class="post-meta">'+
						'<span class="author">作者：<a href="{0}">{1}</a></span> &bull;'.format("#",data[x].user.userName)+
						'<time class="post-date"> {0}</time>'.format(data[x].publishtime.replace("T"," "))+
					'</div>'+
				'</div>'+
				'<div class="post-content">'+
					'<img src="images/rand/{0}.jpg" class="item-thumb" alt="article">'.format(Math.floor(Math.random()*20+1))+
					'<p>{0}</p>'.format(brief(data[x].content))+
				'</div>'+
				' <div class="post-permalink">'+
					'<a href="Showarticle.do?id={0}" class="btn btn-default">阅读全文</a>'.format(data[x].id)+
				'</div>'+
			'</article>';
		$("#fmain").append(xx);
	}
 }

function getarticlecount(){
	var fllg=1;
	if(articlecount==0 && fllg==1){
		  $.ajax({
			  type:"post",
	             cache:false,
	             url:"GetArticlecc.do",
	             dataType:"json",
	             async:true,
	             error:function(request) {
	           	  alert("数目加载出错");
	             },
	             success:function(data){
	            	 articlecount=data['result'];
	             }
			})
			fllg=0;
	}
	return articlecount;
}
