<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/screen.css" rel="stylesheet">
    <link href="css/bootstrapValidator.css" rel="stylesheet">
    
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/marked.min.js"></script>
    <script src="js/highlight/highlight.js"></script>
	<title><s:text name="reg.nav" /></title>
	<script src="js/bootstrapValidator.min.js"></script>
</head>
<body>
	<s:include value="index_header.jsp"/>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">注册</div>
                <div class="panel-body">
                    <form id="regForm" class="form-horizontal" role="form" method="POST" action="regist.do">
                        <div class="form-group">
                            <label for="uname" class="col-md-4 control-label">用户名</label>
                            <div class=" col-md-6">
                                <input id="uname" name="uname" type="text" class="form-control"  value=""
                                       maxlength="10" minlength="6" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pwd" class="col-md-4 control-label">密码</label>
                            <div class="col-md-6">
                                <input id="pwd" type="password" class="form-control" name="pwd" maxlength="30"
                                       minlength="6" required>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pwd2" class="col-md-4 control-label">确认密码</label>

                            <div class="col-md-6">
                                <input id="pwd2" type="password" class="form-control"
                                       name="pwd2" maxlength="30" minlength="6" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">邮箱</label>

                            <div class="col-md-6">
                                <input id="email" name="email" type="email" class="form-control" maxlength="30" minlength="6" required>
                            </div>
                        </div>
                        <div class="form-group form-horizontal">
                            <label for="yzm" class="col-md-4 control-label">验证码</label>
                            <div class="col-md-4">
                                <input id="yzm" name="valcode" type="text" class="form-control col-md-3" maxlength="4" minlength="4" required />
                            </div>
                            <img id="imgsrc" class="img-responsive" onclick="javascript:reloadImage();" src="yzm.jsp"/>
                            <a href="javascript:reloadImage();"> 看不清，换一张吧</a>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                  		  注册
                                </button>
                                <a class="btn btn-link" href="login.jsp">
                                    	已有账号？去登录
                                </a>
                            </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('#regForm').bootstrapValidator({
        message: '输入错误',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            uname: {
                message: '用户名格式错误',
                validators: {
                    notEmpty: {
                        message: '用户名不能为空'
                    },
                    stringLength: {
                        min: 3,
                        max: 15,
                        message: '用户名最少3个字符,或最多15个字符'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: '用户名只能有字母,数字,下划线,小数点组成'
                    },
                    remote:{
                        message: '用户名已被占用，请重新输入',
                        url: 'UCAction.do',
                        data:{
                        	uname : $('#uname').val()
                        },
                        delay:3000
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: '邮箱不能为空'
                    },
                    emailAddress: {
                        message: '邮箱格式错误'
                    }
                }
            },
            pwd: {
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    },
                    stringLength: {
                        min: 6,
                        message: '密码最少6个字符'
                    },
                    identical: {
                        field: 'pwd2',
                        message: '密码不匹配'
                    }
                }
            },
            pwd2: {
                validators: {
                    notEmpty: {
                        message: '密码不能为空'
                    },
                    stringLength: {
                        min: 6,
                        message: '密码最少6个字符'
                    },
                    identical: {
                        field: 'pwd',
                        message: '密码不匹配'
                    }
                }
            },
            valcode:{
                message: '验证码格式错误',
                validators:{
                    notEmpty: {
                        message: '验证码不能为空',
                    },
                    stringLength: {
                        min: 4,
                        max: 4,
                        message: '输入有误，请重新输入'
                    },
                    remote:{
                        message: '验证码错误',
                        url: 'VCAction.do',
                        data:{
                        	valcode : $('#valcode').val()
                        },
                        delay:3000
                    }
                }
            }
        }
    })
});

function reloadImage(){/*重新加载验证码图片*/
	$('#imgsrc').attr('src','yzm.jsp?'+Math.random());
}
</script> 
    
</body>
</html>