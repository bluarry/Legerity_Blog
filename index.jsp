<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/screen.css" rel="stylesheet">
    <link href="css/application.css" rel="stylesheet">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <title>
        <s:text name="index.title"/>
    </title>

</head>
<body>
<s:include value="index_header.jsp"/>


<!-- start 主界面布局 -->
<section class="content-wrap">
    <div class="container">
        <div class="row">
            <!--文章列表-->
            <main id="fmain" class="col-md-9">
            
            </main>
      
            <!--侧边栏-->
            <aside class="col-md-3 sidebar">
                <div class="widget">
                    <h4 class="title">热门文章</h4>
                    <div class="content community">
                        <p>
                            <a href="#" title="文章标题" target="_blank">第一篇文章</a>
                        </p>
                        <p>
                            <a href="#" title="文章标题" target="_blank">第一篇文章</a>
                        </p>
                    </div>
                </div>
                <!-- 
                <div class="widget">
                    <h4 class="title">标签</h4>
                    <div class="content tag-cloud">
                        <a href="#">客户端</a> <a href="#">Android</a> <a href="#">学习</a> <a
                            href="#">算法</a>
                </div>
                 -->
            </aside>
      
        </div>
    </div>
</section>
 <script type="text/javascript" src="js/indexarticles.js"></script>
<script type="text/javascript">
	var count=1;
	var flag = true;                                                //标志，确定达到之后则停止,为了同时只进行一次加载操作  
	$(document).ready(function(){
			  $.ajax({
				  type:"post",
	           cache:false,
	           url:"GetArticle.do",
	           dataType:"json",
	           data:"count="+count,
	           async:true,
	           error:function(request) {
	         	  alert("开始加载出错");
	           },
	           success:dealjsonArticle
				})
			count=count+1;
	  });

	$(window).scroll(function () {loadmore($(this));});  
	articlecount=getarticlecount();
	function loadmore(obj) {  
	    var scrollTop = $(obj).scrollTop();                         //目前所在的位置  
	    var scrollHeight = $(document).height();                    //文章一共有多长  
	    var windowHeight = $(obj).height();                         //一页一共有多长  
	    if (scrollHeight - (scrollTop + windowHeight) <= 20) {      //20的意思是预留长度，总不能翻页到最底下才加载把~  
	    	if (flag) {  
	        	  $.ajax({
	       			  type:"post",
	                     cache:false,
	                     url:"GetArticle.do",
	                     dataType:"json",
	                     data:"count="+count,
	                     async:true,
	                     error:function(request) {
	                   	  alert("加载出错");
	                     },
	                     success:dealjsonArticle
	       			})
	       		if(articlecount<count){flag=false;}
	        }
	     count=count+1;
	    }  
	}    
</script>

</body>
</html>