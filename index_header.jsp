<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>

<!-- start 头图片 -->
<header class="main-header iheaderstyle">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>博客系统</h1>
			</div>
		</div>
	</div>
</header>
<!-- end 头图片 -->

<!-- start 导航 -->

<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target=".navbar-collapse"
				aria-expanded="false">
				<span class="sr-only">菜单</span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li id="Menu_index" class="active"><a href="index.do"><s:text name="menu.index"/></a></li>
				<li id="Menu_about"><a href="About.do"><s:text name="menu.about"/></a></li>
			</ul>
			<%--判断是否已经登录过了 --%>
			<s:if test="#session.role!=null && #session.role>=1&& #session.role<=3">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="javascript:void(0)"><s:text name="%{#session.uname}"></s:text></a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"><s:text name="msg.me"/> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="admin_index.do"><s:text name="msg.admin_back"/></a></li>
							<li role="separator" class="divider"></li>
							<li><a href="logout.do?baction=logout"><s:text name="msg.logout"/></a></li>
						</ul></li>
				</ul>
			</s:if>
			<s:else>
				 <ul class="nav navbar-nav navbar-right">
                <li><a href="reg.jsp"><s:text name="reg.nav"/></a></li>
                <li><a href="login.jsp"><s:text name="msg.dl"/></a></li>
            </ul>
			</s:else>

		</div>
	</div>
</nav>
<!-- end 导航 -->
